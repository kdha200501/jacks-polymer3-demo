## Introduction

1. demonstrates [`jacks-polymer3-cli`](https://www.npmjs.com/package/jacks-polymer3-cli) features:

     - separates style from Web Components into Sass file
     - separates layout from Web Components into HTML file
     - organises code base into modules
     - in `dev` build, transpiles ES6 into ES5 at run time using [System Js](https://www.npmjs.com/package/systemjs) and Babel
     - in `prod` build, transpiles, tree-shakes, bundles and minifies ES6 into ES5 at build time, lazy-loads modules at run time using [System Js Builder](https://www.npmjs.com/package/systemjs-builder)
     - imports libraries such as `polymer`, `rxjs`, `Lodash` and `es6-enum` using ES6 import syntax

2. demonstrates that, Web Components can be used along side with standard HTML elements, see `src/SignUp/index.html`

## How to install the demo

```
$ git clone https://kdha200501@bitbucket.org/kdha200501/jacks-polymer3-demo.git

$ cd jacks-polymer3-demo

$ npm install
```

## How to make a `dev` build and watch changes

```
$ npm start
```

> navigate to [the demo page](http://localhost:8081/SignUp)

> navigate to a [reusable component page](http://localhost:8081/Footnote)


## How to make a `dev` build, only

```
$ npm run build-dev
```

## How to make a `prod` build

```
$ npm run build-prod
```
serve up the `prod` build by:

```
$ cd dist
$ python -m SimpleHTTPServer 8081
```

Code Pen [demo page](https://codepen.io/kdha200501/full/NLGjXQ/)

Code Pen [reusable component page](https://codepen.io/kdha200501/full/rZOmKy/)