import Enum from 'es6-enum';

import {VALIDATION_TYPE, validationMap} from '../../Validation/Module';

export const INPUT_TYPE = Enum(
    'FIRST_NAME',
    'LAST_NAME',
    'USER_NAME',
    'EMAIL',
    'PRIMARY_EMAIL',
    'SECONDARY_EMAIL',
    'PASSWORD',
    'PHONE_NUMBER',
    'MOBILE_PHONE_NUMBER',
    'USER_ROLE',
    'SUBMIT'
);

// maps each input type to a list of validations
const inputValidationMap = {};
inputValidationMap[INPUT_TYPE.FIRST_NAME] = [
    VALIDATION_TYPE.IS_REQUIRED,
    VALIDATION_TYPE.MAX_LEN_50
];
inputValidationMap[INPUT_TYPE.LAST_NAME] = [
    VALIDATION_TYPE.IS_REQUIRED,
    VALIDATION_TYPE.MAX_LEN_50
];
inputValidationMap[INPUT_TYPE.USER_NAME] = [
    VALIDATION_TYPE.IS_REQUIRED
];
inputValidationMap[INPUT_TYPE.EMAIL] = [
    VALIDATION_TYPE.IS_REQUIRED,
    VALIDATION_TYPE.HAS_AT_AND_DOT,
    VALIDATION_TYPE.MAX_LEN_100
];
inputValidationMap[INPUT_TYPE.PRIMARY_EMAIL] = [
    VALIDATION_TYPE.IS_REQUIRED,
    VALIDATION_TYPE.HAS_AT_AND_DOT,
    VALIDATION_TYPE.MAX_LEN_100
];
inputValidationMap[INPUT_TYPE.SECONDARY_EMAIL] = [
    VALIDATION_TYPE.HAS_AT_AND_DOT,
    VALIDATION_TYPE.MAX_LEN_100
];
inputValidationMap[INPUT_TYPE.PASSWORD] = [
    VALIDATION_TYPE.IS_REQUIRED,
    VALIDATION_TYPE.MIN_LEN_10,
    VALIDATION_TYPE.NUM_MIN_LEN_3,
    VALIDATION_TYPE.SPECIAL_CHAR_MIN_LEN_2
];
inputValidationMap[INPUT_TYPE.USER_ROLE] = [
    VALIDATION_TYPE.IS_REQUIRED
];

export class InputValidation {

    constructor(inputType) {
        this._validationTypeList = inputValidationMap[INPUT_TYPE[inputType]] || [];
        this._hasError = false;
        this._validationType = null;
    }

    // Accessors

    get hasError() {
        return this._hasError;
    }

    get validationType() {
        return this._validationType;
    }

    set value(text) {
        this._hasError = false;
        this._validationType = null;
        for(let validationType of this._validationTypeList) {
            if( validationMap[validationType](text) ) {
                this._hasError = true;
                this._validationType = validationType;
                break;
            }
        }
        // endEach type of validation
    }
}
