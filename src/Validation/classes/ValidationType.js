import isString from 'lodash/lang/isString';
import Enum from 'es6-enum';

export const VALIDATION_TYPE = Enum(
    'IS_REQUIRED',
    'MIN_LEN_1',
    'MIN_LEN_2',
    'MIN_LEN_10',
    'MAX_LEN_50',
    'MAX_LEN_100',
    'NUM_MIN_LEN_3',
    'NUM_MAX_LEN_0',
    'SPECIAL_CHAR_MIN_LEN_2',
    'HAS_AT_AND_DOT',
);

const minLength = (text, min) => text.length < min;

const maxLength = (text, max) => text.length > max;

const countDigitsInString = text => text
    .split('')// not using regular expression with 'g' flag to remove non-digits, because js saves state between calls
    .filter(char => /\d/.test(char))
    .length;

const countSpecialCharInString = text => text
    .split('')// reference: https://www.owasp.org/index.php/Password_special_characters
    .filter(char => /[\s!"#$%&'()*+,-./:;<=>?@\[\\\]^_`{|}~]/.test(char))
    .length;

const anAtFollowedByDot = text => /(?=.*\b@\b)(?=.*\b\.\b).*/.test(text);

// maps validation type to implementation
export const validationMap = {};
validationMap[VALIDATION_TYPE.IS_REQUIRED] =            text => !isString(text) || minLength(text, 1);
validationMap[VALIDATION_TYPE.MIN_LEN_1] =              text => text.length > 0 && minLength(text, 1);
validationMap[VALIDATION_TYPE.MIN_LEN_2] =              text => text.length > 0 && minLength(text, 2);
validationMap[VALIDATION_TYPE.MIN_LEN_10] =              text => text.length > 0 && minLength(text, 10);
validationMap[VALIDATION_TYPE.MAX_LEN_50] =             text => text.length > 0 && maxLength(text, 50);
validationMap[VALIDATION_TYPE.MAX_LEN_100] =            text => text.length > 0 && maxLength(text, 100);
validationMap[VALIDATION_TYPE.NUM_MIN_LEN_3] =          text => text.length > 0 && countDigitsInString(text) < 3;
validationMap[VALIDATION_TYPE.NUM_MAX_LEN_0] =          text => text.length > 0 && countDigitsInString(text) > 0;
validationMap[VALIDATION_TYPE.SPECIAL_CHAR_MIN_LEN_2] = text => text.length > 0 && countSpecialCharInString(text) < 2;
validationMap[VALIDATION_TYPE.HAS_AT_AND_DOT] =         text => text.length > 0 && !anAtFollowedByDot(text);
