import {PolymerElement, html} from '@polymer/polymer';
import Rx from 'rxjs/Rx';

import {DemoCreateAccountFormService} from '../classes/DemoCreateAccountFormService';

export class DemoSignUpFormSubmit extends PolymerElement {

    static get is() {
        return 'demo-sign-up-form-submit';
    };

    static get template() {
        return html`{
            templateUrl:    './DemoSignUpFormSubmit.html',
            styleUrl:       './DemoSignUpFormSubmit.scss'
        }`;
    }

    constructor() {
        super();
        this.demoCreateAccountFormService = DemoCreateAccountFormService.getInstance();
    }

    ready() {
        this.addEventListener('click', this.gatherValidations, true);//     [capture: true] executes the callback
        this.addEventListener('keypress', this.gatherValidations, true);//  function prior to the target element's own
                                                                        //  onClick handler
        this.demoCreateAccountFormService.formSubmitObservable = Rx.Observable.merge(
            Rx.Observable.fromEvent(this, 'mousedown'),//   broadcasts to instruct subscribers to validate inputs prior
            Rx.Observable.fromEvent(this, 'keydown')//      to click and keypress events
        );// TODO: test mobile

        super.ready();
    }

    gatherValidations(event) {
        if(this.demoCreateAccountFormService.hasError) {
            event.stopPropagation();
            event.preventDefault();// disables the target element's own onClick handler i.e. form submission
        }
    }
}
