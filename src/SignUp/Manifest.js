import {DemoSignUpForm} from './DemoSignUpForm/DemoSignUpForm';
import {DemoSignUpFormInput} from './DemoSignUpFormInput/DemoSignUpFormInput';
import {DemoSignUpFormError} from './DemoSignUpFormError/DemoSignUpFormError';
import {DemoSignUpFormSelect} from './DemoSignUpSelect/DemoSignUpFormSelect';
import {DemoSignUpFormSubmit} from './DemoSignUpSubmit/DemoSignUpFormSubmit';

export const WebComponentList = [
    DemoSignUpForm,
    DemoSignUpFormInput,
    DemoSignUpFormError,
    DemoSignUpFormSelect,
    DemoSignUpFormSubmit
];
