import {PolymerElement, html} from '@polymer/polymer';

import {DemoCreateAccountFormService} from '../classes/DemoCreateAccountFormService';
import {InputValidation} from '../../Validation/Module';

export class DemoSignUpFormSelect extends PolymerElement {

    static get is() {
        return 'demo-sign-up-form-select';
    };

    static get template() {
        return html`{
            templateUrl:    './DemoSignUpFormSelect.html',
            styleUrl:       './DemoSignUpFormSelect.scss'
        }`;
    }

    constructor() {
        super();
        this.demoCreateAccountFormService = DemoCreateAccountFormService.getInstance();
        this._isPrestine = true;
        this._validation = new InputValidation(this.slot);
        this.hideError = true;
        this.validationType = null;
    }

    ready() {
        let self = this;
        this.addEventListener('keyup', this.validate);
        setTimeout(() => {
            this.demoCreateAccountFormService.formSubmitObservable.subscribe(
                (event) => {
                    self._isPrestine = false;
                    self.validate();
                },
                (error) => {},
                (e)=> {}
            );
            self.querySelector('select').addEventListener('blur', () => {
                self._isPrestine = false;
                self.validate();
            });
            self.querySelector('select').addEventListener('change', () => {
                self._isPrestine = false;
                self.validate();
            });
        }, 0);

        super.ready();
    }

    // Public methods

    validate() {
        // trigger error state update
        this._validation.value = this.querySelector('select').value;
        this.demoCreateAccountFormService.hasError = this.demoCreateAccountFormService.hasError || this._validation.hasError;

        // trigger view update
        this.hideError = this._validation.hasError ? this._isPrestine : true;
        this.validationType = this._validation.validationType;
    }
}
