import {PolymerElement, html} from '@polymer/polymer';
import {VALIDATION_TYPE} from '../../Validation/Module';

export class DemoSignUpFormError extends PolymerElement {

    static get is() {
        return 'demo-sign-up-form-error';
    };

    static get template() {
        return html`{
            templateUrl:    './DemoSignUpFormError.html',
            styleUrl:       './DemoSignUpFormError.scss'
        }`;
    }

    static get properties () {
        return {
            validationType: {
                type: Symbol
            }
        };
    }

    IS_REQUIRED(validationType)               {return validationType === VALIDATION_TYPE.IS_REQUIRED;};
    MIN_LEN_1(validationType)                 {return validationType === VALIDATION_TYPE.MIN_LEN_1;};
    MIN_LEN_2(validationType)                 {return validationType === VALIDATION_TYPE.MIN_LEN_2;};
    MIN_LEN_10(validationType)                 {return validationType === VALIDATION_TYPE.MIN_LEN_10;};
    MAX_LEN_50(validationType)                {return validationType === VALIDATION_TYPE.MAX_LEN_50;};
    MAX_LEN_100(validationType)               {return validationType === VALIDATION_TYPE.MAX_LEN_100;};
    NUM_MIN_LEN_3(validationType)             {return validationType === VALIDATION_TYPE.NUM_MIN_LEN_3;};
    NUM_MAX_LEN_0(validationType)             {return validationType === VALIDATION_TYPE.NUM_MAX_LEN_0;};
    SPECIAL_CHAR_MIN_LEN_2(validationType)    {return validationType === VALIDATION_TYPE.SPECIAL_CHAR_MIN_LEN_2;};
    HAS_AT_AND_DOT(validationType)            {return validationType === VALIDATION_TYPE.HAS_AT_AND_DOT;};
}
