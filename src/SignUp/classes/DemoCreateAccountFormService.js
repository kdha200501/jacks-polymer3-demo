export class DemoCreateAccountFormService {

    // Singleton design pattern
    static _singleton = null;

    static getInstance() {
        if (DemoCreateAccountFormService._singleton === null) {
            return new DemoCreateAccountFormService();
        } else {
            return DemoCreateAccountFormService._singleton;
        }
    }

    constructor() {
        if(DemoCreateAccountFormService._singleton) {
            throw new Error('Error: Instantiation failed: Use DemoCreateAccountFormService.getInstance() instead of new DemoCreateAccountFormService().');
            // endIf singleton already instantiated
        } else {
            DemoCreateAccountFormService._singleton = this;
            // endIf singleton not yet instantiated
        }
        this._formSubmitObservable = null;
        this._formSubmitObserver = null;
        this._hasError = false;
    }

    // Accessors

    get formSubmitObservable() {
        return this._formSubmitObservable;
    }

    set formSubmitObservable(observable) {
        this._formSubmitObservable = observable;
        if(this._formSubmitObserver === null) {
            this._formSubmitObserver = observable.subscribe(
                (event) => {
                    this._hasError = false;// reset error state
                },
                (error) => {},
                (e)=> {}
            );
        }
    }

    get hasError() {
        return this._hasError;
    }

    set hasError(hasError) {
        this._hasError = hasError;
    }
}
