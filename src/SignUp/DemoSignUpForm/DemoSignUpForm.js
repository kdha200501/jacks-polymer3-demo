import {PolymerElement, html} from '@polymer/polymer';

export class DemoSignUpForm extends PolymerElement {

    static get is() {
        return 'demo-sign-up-form';
    };

    static get template() {
        return html`{
            templateUrl:    './DemoSignUpForm.html',
            styleUrl:       './DemoSignUpForm.scss'
        }`;
    }

    ready() {
        super.ready();
        this.style.display = 'block';
    }
}
