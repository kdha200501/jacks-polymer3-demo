import {PolymerElement, html} from '@polymer/polymer';

import {DemoCreateAccountFormService} from '../classes/DemoCreateAccountFormService';
import {InputValidation, INPUT_TYPE} from '../../Validation/Module';

export class DemoSignUpFormInput extends PolymerElement {

    static get is() {
        return 'demo-sign-up-form-input';
    };

    static get template() {
        return html`{
            templateUrl:    './DemoSignUpFormInput.html',
            styleUrl:       './DemoSignUpFormInput.scss'
        }`;
    }

    static stopEventPropagation(event) {
        event.stopPropagation();
    }

    constructor() {
        super();
        this.isPassword = null;
        this.isPasswordVisible = false;
        this.demoCreateAccountFormService = DemoCreateAccountFormService.getInstance();
        this._isPrestine = true;
        this._validation = null;
        this.hideError = true;
        this.validationType = null;
    }

    ready() {
        let self = this;
        this.addEventListener('keypress', DemoSignUpFormInput.stopEventPropagation, true);
        this.addEventListener('keyup', this.validate);
        setTimeout(() => {
            this.demoCreateAccountFormService.formSubmitObservable.subscribe(
                (event) => {
                    self._isPrestine = false;
                    self.validate();
                },
                (error) => {},
                (e)=> {}
            );
            self.querySelector('input').addEventListener('blur', () => {
                self._isPrestine = false;
                self.validate();
            });
        }, 0);
        this.isPassword = INPUT_TYPE[this.slot] === INPUT_TYPE.PASSWORD;
        this._validation = new InputValidation(this.slot);

        super.ready();
    }

    // Public methods

    toggle() {
        this.isPasswordVisible = !this.isPasswordVisible;
        this.querySelector('input').setAttribute('type', this.isPasswordVisible ? 'text' : 'password');
    }

    validate() {
        // trigger error state update
        this._validation.value = this.querySelector('input').value;
        this.demoCreateAccountFormService.hasError = this.demoCreateAccountFormService.hasError || this._validation.hasError;

        // trigger view update
        this.hideError = this._validation.hasError ? this._isPrestine : true;
        this.validationType = this._validation.validationType;
    }
}
