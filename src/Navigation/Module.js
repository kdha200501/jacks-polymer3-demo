import {WebComponentList} from './Manifest';

// Register web components with the browser.
for(let webComponent of WebComponentList) {
    customElements.define(webComponent.is, webComponent);
}
