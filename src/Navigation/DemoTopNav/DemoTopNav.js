import {PolymerElement, html} from '@polymer/polymer';

export class DemoTopNav extends PolymerElement {

    static get is() {
        return 'demo-top-nav';

    };

    static get template() {
        return html`{
            templateUrl:    './DemoTopNav.html',
            styleUrl:       './DemoTopNav.scss'
        }`;
    }

    static get properties() {
        return {
            showNavMenu: {
                type: Boolean,
                observer: 'toggleNavMenu'
            }
        };
    };

    ready() {
        super.ready();
        this.style.display = 'block';
    }

    toggleNavMenu(value) {
    }

    hideNavMenu() {
        this.showNavMenu = false;
    }
}
