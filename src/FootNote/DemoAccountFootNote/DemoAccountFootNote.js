import {PolymerElement, html} from '@polymer/polymer';

export class DemoAccountFootNote extends PolymerElement {

    static get is() {
        return 'demo-account-footnote';

    };

    static get template() {
        return html`{
            templateUrl:    './DemoAccountFootNote.html',
            styleUrl:       './DemoAccountFootNote.scss'
        }`;
    }
}
